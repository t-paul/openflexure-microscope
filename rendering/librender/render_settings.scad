

function extras_colour() = "DodgerBlue";
function tools_colour() = "MediumAquamarine";
function body_colour() = "WhiteSmoke";
function remove_colour() = "Red";
function optics_module_colour() = "#404040";