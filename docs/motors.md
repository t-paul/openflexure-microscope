# Assemble the motors

{{BOM}}


[M4x6mm button head screws]: parts/mechanical.yml#ButtonScrew_M4x6mm_SS
[28BYJ-48 micro geared stepper motors]: parts/electronics.yml#28BYJ-48

## Attach the small gears {pagestep}

* Take a [stepper motor][28BYJ-48 micro geared stepper motors]{qty:3, cat:electronic} and a [small gear][small gears](fromstep){qty:3, cat:printedpart}
* Place the motor on the work surface with the shaft pointing up
* Apply a small drop of [threadlock adhesive]{qty: 6 drops, cat:consumable} to each side of the motor shaft near the top
* Align the flat sides of the motor shaft with the flat sides of the hole in the gear.
* Push the gear onto the motor with the flanged side downwards (motor side).
* Repeat for the other two motors

## Attach the motors {pagestep}

Note that each motor has a cable tidy cap that is different. Which cap to use should be apparent from the shape.

* Get a [2.5mm Allen key]{qty:1, cat:tool} ready
* Feed the cable from the motor through the rectangular wall in the outer wall by the X actuator.
* Place the motor on the motor lugs with the small gear towards the outside of the microscope
* Check that the small gear and the large gear are meshed correctly
* Take the X [cable tidy cap][cable tidy caps](fromstep){qty:3, cat:printedpart, note: "- Each cap is a different shape"} and place it over the motor
* Check that the motor cable is running through the cable tidy rather than pinched underneath.
* Fasten the motor and cable tidy caps to the motor lugs with two [M4x6mm button head screws]{qty:6, cat:mech}
* Repeat for Y and Z actuators. For the Z actuator use the rectangular slot to the left of the actuator.


![](renders/cable_management.png)


