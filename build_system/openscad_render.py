#!/usr/bin/env python3

"""
This module wraps openscad and checks whether a warning happened during render.

This is needed for rendering as `--hardwardings` stops compilations but no exit code is set
instead a blank .png is output. See: https://github.com/openscad/openscad/issues/3616
"""

import subprocess
import sys
import re


def main(args):
    """
    Run openscad with hardwarnings and return any messages from std_error on std_out.
    If OpenSCAD warnings are thrown exit the program with an error.
    """

    #Note not using util.get_openscad_exe to keep this a stand alone script
    if sys.platform.startswith("darwin"):
        executable = "/Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD"
    else:
        executable = "openscad"
    ret = subprocess.run([executable, '--hardwarnings'] + args, check=True, capture_output=True)
    std_err = ret.stderr.decode('UTF-8')
    print(std_err)
    if re.findall(r'^WARNING:', std_err, flags=re.MULTILINE) != []:
        sys.exit(1)

if __name__ == "__main__":
    main(sys.argv[1:])
